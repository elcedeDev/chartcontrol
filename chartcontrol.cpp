#include "chartcontrol.h"

#include <qdeclarative.h>
#include <QPainter>
#include <QDebug>
#include <QDate>
#include "version.h"

ChartControl::ChartControl(QDeclarativeItem *parent):
    QDeclarativeItem(parent)
{
    // By default, QDeclarativeItem does not draw anything. If you subclass
    // QDeclarativeItem to create a visual item, you will need to uncomment the
    // following line:

    setFlag(ItemHasNoContents, false);

    m_BackgroundOuter   = QColor(220,220,220);
    //m_BackgroundOuter   = QColor(255,100,100);
    m_BackgroundInner   = QColor(210,210,210);
    m_TenPercent        = QColor(220,0,0);
    m_TwentyPercent     = QColor(255,200,0);
    m_FiftyPercent      = QColor(0,220,0);

    PRINTVERSION
}

ChartControl::~ChartControl()
{
}

void ChartControl::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->setPen(QPen(m_BackgroundOuter));
    painter->setBrush(QBrush(m_BackgroundOuter));
    painter->drawEllipse(boundingRect());
    painter->setPen(QPen(m_BackgroundInner));
    painter->setBrush(QBrush(m_BackgroundInner));
    painter->drawEllipse(QPointF(boundingRect().width()/2, boundingRect().height()/2),boundingRect().width()/4 + 1,boundingRect().height()/4 + 1);

    QColor color;

    if( m_Percent < 0) m_Percent = 0;

    if( m_Percent <= 10)
        color = m_TenPercent;
    else if( m_Percent <= 20 )
        color = m_TwentyPercent;
    else
        color = m_FiftyPercent;

    double percent = m_Percent;

    painter->setPen(QPen(color, boundingRect().width()/4,Qt::SolidLine ,Qt::RoundCap, Qt::RoundJoin));
    painter->drawArc(QRectF(QPointF(boundingRect().x() + boundingRect().width()/8 - 1,
                                    boundingRect().y() + boundingRect().height()/8 - 1),
                            QPointF(boundingRect().bottomRight().x() - boundingRect().width()/8 + 1,
                                    boundingRect().bottomRight().y() - boundingRect().height()/8 +1 )),
                            90*16, - (percent/100)*360*16);
                            //90*16, - 275*16);
    painter->setPen(QColor(0,0,0));
    QFont font = QFont("Roboto Bold");
    font.setPointSizeF(boundingRect().height()*2/12);

    painter->setFont(font);
    QString str = QString("%1%").arg(m_Percent);
    painter->drawText(QRectF(boundingRect()), Qt::AlignCenter, str);
    //qDebug() << "ChartControl::paint -- boundingRect()" << boundingRect() << "boundingRect().height()" << boundingRect().height();
    //qDebug() << "ChartControl::paint -- m_Percent/100" << percent/100 << "(m_Percent/100)*360" << (percent/100)*360;
}

int ChartControl::percent() const
{
    return m_Percent;
}

void ChartControl::setPercent(int i)
{
    if(i > 100) m_Percent = 100;
    else    m_Percent = abs(i);

}

