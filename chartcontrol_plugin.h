#ifndef CHARTCONTROL_PLUGIN_H
#define CHARTCONTROL_PLUGIN_H

#include <QDeclarativeExtensionPlugin>

class ChartControlPlugin : public QDeclarativeExtensionPlugin
{
    Q_OBJECT
#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "ChartControl")
#endif

public:
    void registerTypes(const char *uri);
};

#endif // CHARTCONTROL_PLUGIN_H

