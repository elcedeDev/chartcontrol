#include "chartcontrol_plugin.h"
#include "chartcontrol.h"

#include <qdeclarative.h>
#include <QtPlugin>

void ChartControlPlugin::registerTypes(const char *uri)
{
    // @uri elcede.qmlcomponents
    qmlRegisterType<ChartControl>(uri, 1, 0, "ChartControl");
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(ChartControl, ChartControlPlugin)
#endif

