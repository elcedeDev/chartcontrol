#ifndef CHARTCONTROL_H
#define CHARTCONTROL_H

#include <QObject>
#include <QDeclarativeItem>
#include <QColor>
#include <QBitArray>
#include <QPainterPath>


class ChartControl : public QDeclarativeItem
{
    Q_OBJECT
    Q_DISABLE_COPY(ChartControl)
    Q_PROPERTY(int percent READ percent WRITE setPercent)

public:
    ChartControl(QDeclarativeItem *parent = 0);
    ~ChartControl();


    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);


    int percent() const;
    void setPercent(int);


protected:

private:
    int                             m_Percent;
    QColor                          m_BackgroundOuter;
    QColor                          m_BackgroundInner;
    QColor                          m_TenPercent;
    QColor                          m_TwentyPercent;
    QColor                          m_FiftyPercent;
};



#endif // CHARTCONTROL_H
